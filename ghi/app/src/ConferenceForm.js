import React from 'react';

class ConferenceFrom extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: []
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.locations;
        const ConferencesUrl = "http://localhost:8000/api/conferences/";
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(ConferencesUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json();
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            };
            this.setState(cleared);
        } else {
            throw new Error('response is not ok')
        }
    }
    async componentDidMount() {
        const locationsUrl = "http://localhost:8000/api/locations/";
        const locationsResponse = await fetch(locationsUrl)
        if (locationsResponse.ok) {
            const data = await locationsResponse.json();
            this.setState({ 'locations': data.locations })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Conference</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Name"
                                    required type="text"
                                    name="name"
                                    className="form-control"
                                    value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Starts"
                                    required type="date"
                                    name="starts"
                                    className="form-control"
                                    value={this.state.starts}
                                />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Ends"
                                    required type="date"
                                    name="ends"
                                    className="form-control"
                                    value={this.state.ends}
                                />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description">Description</label>
                                <textarea
                                    onChange={this.handleInputChange}
                                    required type="textarea"
                                    id="description"
                                    name="description"
                                    rows="5"
                                    className="form-control"
                                    value={this.state.description}>
                                </textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Maximum Presentations"
                                    required type="number"
                                    name="max_presentations"
                                    className="form-control"
                                    value={this.state.max_presentations}
                                />
                                <label htmlFor="max_presentations">Maximum Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Maximum Attendees"
                                    required type="number"
                                    name="max_attendees"
                                    className="form-control"
                                    value={this.state.max_attendees}
                                />
                                <label htmlFor="max_attendees">Maximum Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleInputChange}
                                    required id="location"
                                    name="location"
                                    className="form-select"
                                    value={this.state.location}>
                                    <option value="">Choose a Location</option>
                                    {this.state.locations.map((state) => {
                                        return (
                                            <option key={state.id} value={state.id}>
                                                {state.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}


export default ConferenceFrom;
