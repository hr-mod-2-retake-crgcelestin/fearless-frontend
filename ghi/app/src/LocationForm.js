import React from 'react';

class LocationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            room_count: '',
            city: '',
            states: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleRoomCountChange = this.handleRoomCountChange.bind(this)
        this.handleCityChange = this.handleCityChange.bind(this)
        this.handleStateChange = this.handleStateChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleRoomCountChange(event) {
        const value = event.target.value;
        this.setState({ room_count: value })
    }
    handleCityChange(event) {
        const value = event.target.value;
        this.setState({ city: value })
    }
    handleStateChange(event) {
        const value = event.target.value;
        this.setState({ state: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.states;
        const locationsUrl = "http://localhost:8000/api/locations/";
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(locationsUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json();
            const cleared = {
                name: '',
                room_count: '',
                city: '',
                state: '',
            };
            this.setState(cleared);
        } else {
            throw new Error('response is not ok')
        }

    }
    async componentDidMount() {
        const statesUrl = "http://localhost:8000/api/states/";
        const statesResponse = await fetch(statesUrl);
        if (statesResponse.ok) {
            const data = await statesResponse.json();
            this.setState({ "states": data.states })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Location</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-location-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleNameChange}
                                    placeholder="Name"
                                    required type="text"
                                    name="name"
                                    className="form-control"
                                    value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleRoomCountChange}
                                    placeholder="Room count"
                                    required type="number"
                                    name="room_count"
                                    className="form-control"
                                    value={this.state.room_count} />
                                <label htmlFor="room_count">Room count</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleCityChange}
                                    placeholder="City"
                                    required type="text"
                                    name="city"
                                    className="form-control"
                                    value={this.state.city}
                                />
                                <label htmlFor="city">City</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleStateChange}
                                    required id="state"
                                    name="state"
                                    className="form-select"
                                    value={this.state.state}>
                                    <option value="">Choose a state</option>
                                    {this.state.states.map((state) => {
                                        return (
                                            <option key={state.abbreviation} value={state.abbreviation}>
                                                {state.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default LocationForm;
