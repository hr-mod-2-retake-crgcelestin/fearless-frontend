import React from 'react';
import Nav from './Nav';

class AttendeesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            attendees: []
        };
    }
    async componentDidMount() {
        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const attendeesResponse = await fetch(attendeesUrl)
        if (attendeesResponse.ok) {
            const data = await attendeesResponse.json();
            this.setState({ 'attendees': data.attendees })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="my-5 container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Conference</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.attendees.map((attendee) => {
                            return (
                                <tr key={attendee.href}>
                                    <td>{attendee.name}</td>
                                    <td>{attendee.conference}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AttendeesList;
