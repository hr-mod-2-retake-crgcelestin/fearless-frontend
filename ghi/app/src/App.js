import React from 'react';
import AttendeesList from './AttendeesList';
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceFrom from './ConferenceForm'
import AttendeeSignUpForm from './AttendeeSignUp';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='attendees'>
            <Route path='list' element={<AttendeesList />} />
            <Route path="new" element={<AttendeeSignUpForm />} />
          </Route>
          <Route path='conferences'>
            <Route path='new' element={<ConferenceFrom />} />
          </Route>
          <Route path="presentations">
            <Route path='new' element={<PresentationForm />} />
          </Route>
          <Route path='locations'>
            <Route path='new' element={<LocationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
