import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            conferences: [],
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.conferences;
        const PresentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(PresentationUrl, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json();
            const cleared = {
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            };
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    async componentDidMount() {
        const conferencesUrl = "http://localhost:8000/api/conferences/"
        const conferenceResponse = await fetch(conferencesUrl)
        if (conferenceResponse.ok) {
            const data = await conferenceResponse.json()
            this.setState({ 'conferences': data.conferences })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Presentation</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Presenter Name"
                                    required type="text"
                                    name="presenter_name"
                                    className="form-control"
                                    value={this.state.presenter_name} />
                                <label htmlFor="presenter_name">Presenter Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Presenter Email"
                                    required type="email"
                                    name="presenter_email"
                                    className="form-control"
                                    value={this.state.presenter_email} />
                                <label htmlFor="email">Presenter Email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Company Name"
                                    required type="text"
                                    name="company_name"
                                    className="form-control"
                                    value={this.state.company_name} />
                                <label htmlFor="company_name">Company Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Title"
                                    required type="text"
                                    name="title"
                                    className="form-control"
                                    value={this.state.title} />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea
                                    onChange={this.handleInputChange}
                                    required type="textarea"
                                    id="synopsis"
                                    name="synopsis"
                                    rows="5"
                                    className="form-control"
                                    value={this.state.synopsis}
                                ></textarea>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleInputChange}
                                    name="conference"
                                    id="conference"
                                    className="form-select"
                                    value={this.state.conference}
                                    required>
                                    <option value="">Choose a Conference</option>
                                    {this.state.conferences.map(state => {
                                        return (
                                            <option key={state.id} value={state.id}>
                                                {state.name}
                                            </option>
                                        )
                                    })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default PresentationForm;
