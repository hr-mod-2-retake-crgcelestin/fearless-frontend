window.addEventListener('DOMContentLoaded', async () => {
    const ConferencesUrl = "http://localhost:8000/api/conferences/"
    try {
        const ConferenceResponse = await fetch(ConferencesUrl)
        if (ConferenceResponse.ok) {
            const conferenceData = await ConferenceResponse.json()
            const selectTag = document.getElementById('conference')
            for (let conference of conferenceData.conferences) {
                let option = document.createElement('option')
                option.value = conference.id;
                option.innerHTML = conference.name;
                selectTag.appendChild(option)
            }
            // add 'd-none' class to loading icon
            // remove 'd-none' class from select tag
            selectTag.classList.remove('d-none')
            const LoadingTag = document.getElementById('loading-conference-spinner')
            LoadingTag.classList.add('d-none')
        } else {
            throw new Error('response is not ok')
        }
    } catch (e) {
        console.error(e)
    }
})

window.addEventListener("DOMContentLoaded", async () => {
    const ConferencesUrl = "http://localhost:8000/api/conferences/"
    try {
        const response = await fetch(ConferencesUrl)
        if (response.ok) {
            const formTag = document.getElementById("create-attendee-form")
            const selectTag = document.getElementById('conference')
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const attendeeUrl = 'http://localhost:8001/api/attendees/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const newAttendeeResponse = await fetch(attendeeUrl, fetchConfig)
                const selectAlert = document.getElementById('success-message')
                selectAlert.classList.remove('d-none')
                formTag.classList.add('d-none')
                if (newAttendeeResponse.ok) {
                    formTag.reset()
                    const newAttendee = await newAttendeeResponse.json()
                } else {
                    throw new Error('Attendee response is not ok')
                }
            })
        } else {
            throw new Error('reponse is not ok')
        }
    } catch (e) {
        console.error(e)
    }
})
