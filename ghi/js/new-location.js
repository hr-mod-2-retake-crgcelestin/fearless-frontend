window.addEventListener('DOMContentLoaded', async () => {
    const statesUrl = "http://localhost:8000/api/states/";
    try {
        const statesResponse = await fetch(statesUrl);
        if (statesResponse.ok) {
            const data = await statesResponse.json();
            const selectTag = document.getElementById("state");
            //use of and not in, in goes through keys vs of going through values
            for (let state of data.states) {
                let option = document.createElement('option')
                option.value = state.abbreviation
                option.innerHTML = state.name
                selectTag.append(option)
            }
        } else {
            throw new Error('response is not ok');
        }
    } catch (e) {
        console.error(e);
    }
})

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states";
    try {
        const statesResponse = await fetch(url)
        if (statesResponse.ok) {
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                //puts inputted data from object formdata into json string format
                const json = JSON.stringify(Object.fromEntries(formData));
                const locationsUrl = "http://localhost:8000/api/locations/";
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const newLocationsResponse = await fetch(locationsUrl, fetchConfig)
                if (newLocationsResponse.ok) {
                    //resets form to original state
                    formTag.reset()
                    const newLocation = await newLocationsResponse.json()
                } else {
                    throw new Error('response is not ok')
                }
            }
            )
        }
    } catch (e) {
        console.error(e);
    }
})
