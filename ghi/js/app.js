function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
    <div class="col">
    <div class="card shadow p-3 mb-5 bg-white rounded">
    <div class="card h-100">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 clas="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted">${starts} - ${ends}</small>
        </div>
    </div>
    </div>
    </div>
    `;
}

function alert(message, e) {
    var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + ' alert-dismissible" role="alert">' + message + e + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
    alertPlaceholder.append(wrapper)
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('reponse is not ok')
        } else {
            //fetch for conference name and placing it in the FE
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000/${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const conference = details.conference
                    const title = conference.name;
                    const description = conference.description;
                    const pictureUrl = conference.location.picture_url
                    const location = conference.location.name
                    const starts = new Date(conference.starts).toLocaleDateString()
                    const ends = new Date(conference.ends).toLocaleDateString()
                    const html = createCard(title, location, description, pictureUrl, starts, ends);
                    const column = document.querySelector('.card-group');
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {
        alert('No conference data', e)
        //console.error('error',e)
    }
});
