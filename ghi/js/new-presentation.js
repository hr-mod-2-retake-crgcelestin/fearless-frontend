window.addEventListener('DOMContentLoaded', async () => {
    const ConferencesUrl = "http://localhost:8000/api/conferences/"
    try {
        const ConferenceResponse = await fetch(ConferencesUrl)
        if (ConferenceResponse.ok) {
            const conferenceData = await ConferenceResponse.json()
            const selectTag = document.getElementById('conference')
            for (let conference of conferenceData.conferences) {
                let option = document.createElement('option')
                option.value = conference.id;
                option.innerHTML = conference.name;
                selectTag.append(option)
            }
        } else {
            throw new Error('response is not ok')
        }
    } catch (e) {
        console.error(e)
    }
})

window.addEventListener('DOMContentLoaded', async () => {
    const ConferencesUrl = "http://localhost:8000/api/conferences/"
    try {
        const response = await fetch(ConferencesUrl)
        if (response.ok) {
            const formTag = document.getElementById('create-presentation-form')
            const selectTag = document.getElementById('conference')
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                //formData.get('conference')
                const conference_id = selectTag.options[selectTag.selectedIndex].value;
                const presentationUrl = `http://localhost:8000/api/conferences/${conference_id}/presentations/`;
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const newPresentationResponse = await fetch(presentationUrl, fetchConfig);
                if (newPresentationResponse.ok) {
                    formTag.reset()
                    const newPresentation = await newPresentationResponse.json()
                } else {
                    throw new Error('response is not ok')
                }
            })
        } else {
            throw new Error('response is not ok')
        }
    } catch (e) {
        console.error(e)
    }
})
