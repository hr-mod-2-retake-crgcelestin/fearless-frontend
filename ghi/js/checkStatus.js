window.addEventListener('DOMContentLoaded', async () => {
    const payloadCookie = await cookieStore.get('jwt_access_payload');
    if (payloadCookie) {
        const encoded = payloadCookie.value
        const decodedPayload = atob(encoded)
        const payload = JSON.parse(decodedPayload)
        const permissions = payload.user.perms
        if (permissions.includes('events.add_conference')) {
            const selectTag = document.querySelector("a[href='new-conference.html']");
            selectTag.classList.remove('d-none')
        }
        if (permissions.includes('events.add_location')) {
            const selectTag = document.querySelector("a[href='new-location.html']");
            selectTag.classList.remove('d-none')
        }
    } else {
        throw new Error('payload cookie does not exist')
    }
})
