window.addEventListener('DOMContentLoaded', async () => {
    const LocationsUrl = "http://localhost:8000/api/locations/";
    try {
        const LocationsResponse = await fetch(LocationsUrl);
        if (LocationsResponse.ok) {
            const data = await LocationsResponse.json();
            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                let option = document.createElement('option')
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.append(option)
            }
        } else {
            throw new Error("response is not ok")
        }
    } catch (e) {
        console.error(e)
    }
})

window.addEventListener('DOMContentLoaded', async () => {
    const LocationsUrl = "http://localhost:8000/api/locations/";
    try {
        const LocationsResponse = await fetch(LocationsUrl);
        if (LocationsResponse.ok) {
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData))
                const ConferencesUrl = "http://localhost:8000/api/conferences/";
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const newConferencesResponse = await fetch(ConferencesUrl, fetchConfig)
                if (newConferencesResponse.ok) {
                    formTag.reset()
                    const newConference = await newConferencesResponse.json()
                } else {
                    throw new Error('response is not ok')
                }
            })
        }
    } catch (e) {
        console.error(e)
    }
})
